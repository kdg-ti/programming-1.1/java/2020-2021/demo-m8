package be.kdg.prog1.demo;

public class Helicopter extends Vehicle {
    private int verticalSpeed;

    public Helicopter(int topSpeed, int verticalSpeed) {
        super(topSpeed);
        this.verticalSpeed = verticalSpeed;
    }

    @Override
    public void print() {
        System.out.println("TOP SPEED: " + getTopSpeed());
        System.out.println("ELECTRIC: " + verticalSpeed);
    }
}
