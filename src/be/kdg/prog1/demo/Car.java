package be.kdg.prog1.demo;

public /*abstract*/ class Car extends Vehicle implements Wheeled {
    private boolean isElectric;

    public static final int WHEELS = 4;
    private static final boolean HAS_ENGINE = true;

    public Car(int topSpeed, boolean isElectric) {
        super(topSpeed);
        this.isElectric = isElectric;
    }

    public static boolean hasEngine() {
        // I don't have an instance here, so
        // I can't call instance methods.
        //System.out.println(this.isElectric());
        return HAS_ENGINE;
    }

    @Override
    public void print() {
        System.out.println("TOP SPEED: " + /*this.*/getTopSpeed());
        System.out.println("ELECTRIC: " + isElectric);
    }

    public boolean isElectric() {
        return isElectric;
    }

    @Override
    public int getNumberOfWheels() {
        return WHEELS;
    }

    // IF abstract, then there is an abstract method in this class called 'getNumberOfWheels'
}
