package be.kdg.prog1.demo;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car(200, false);
        Car car2 = new Car(220, true);
        Car car3 = new Car(230, false);
        // In memory I will have 3 ints (topSpeeds)
        //   and 3 booleans (isElectric)

        // WHEELS is only 'in memory' once

        System.out.println(car3.isElectric());
        //System.out.println(car3.isElectric); // IF public

        System.out.println(Car.WHEELS); // class attibute
        //System.out.println(car3.WHEELS); // class attibute

        System.out.println(Car.hasEngine()); // class method
        //System.out.println(car2.hasEngine()); // class method

        printSomething();

        Object object = new Car(200, false);




        // -->
        if (object instanceof Car) {
            // Cast it to a car and access the getters...
        } else {
            // false
        }
    }

    public static void printSomething() {
        System.out.println("Hello");
    }
}
