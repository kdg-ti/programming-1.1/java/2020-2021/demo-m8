package be.kdg.prog1.demo;

public abstract class Vehicle {
    private int topSpeed;

    public Vehicle(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    public abstract void print();
}
